<%-- 
    Document   : busqueda
    Created on : 7/06/2021, 11:26:35 AM
    Author     : FerMo
--%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.util.Base64"%>
<%@page import="java.sql.*"%>
<%@page import="clases.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Main</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

        <link rel="shortcut icon" type="image/x-icon" href="./images/askandcook.png">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <!--Footer-->
        <!-- Font Awesome -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css"
            rel="stylesheet"
            />
        <!--Footer-->

        <style>
            .bg{
                background-image: url(./images/cooking.jpg);
                background-position: center center;
                background-size: 600px 600px;
                border-radius: 10px 0px 0px 10px;
            }
        </style>

    </head>
    <body>
        <%
            Connection con = null;
            Statement st = null;
            ResultSet rs = null;
            String servername = "localhost";
            String port = "3306";
            String databaseName = "db_ask_n_cook";
            String url = "jdbc:mysql://" + servername + ":" + port + "/" + databaseName;
            out.print("condition 1" + url);
            int UserLoggedI;
            String UserLogged = "0";
            User u = (User) session.getAttribute("User");
            if (u != null) {
                UserLoggedI = u.getId();
                UserLogged = Integer.toString(UserLoggedI);

            } else {
                UserLoggedI = 0;
            }
            String busqueda ="";

        %>

        <%  if (request.getParameter("enviarbus") != null) {
                busqueda = request.getParameter("buscarpreg");
            }
        %>



        <!--Header-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="position: fixed; width: 100%; top:0px; z-index: 1000;">
            <div class="container-fluid">
                <a class="navbar-brand" href="principal.jsp"><img src="./images/askandcook.png" width="50" alt="Error de carga"></a>
                <button class="navbar-toggler " type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon bg-dark"></span>
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="principal.jsp">Inicio</a>

                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" <%if (UserLoggedI == 0) {%> href="inicio.jsp"  <%} else {%> href="perfilpropio.jsp?idu=<%=UserLoggedI%>"<%}%>>Perfil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="busquedap.jsp">Busqueda Avanzada</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Categorias
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <%

                                    try {
                                        Class.forName("com.mysql.jdbc.Driver");

                                        con = DriverManager.getConnection(url, "root", "root");
                                        st = con.createStatement();
                                        rs = st.executeQuery("SELECT * FROM categorias");

                                        while (rs.next()) {
                                %>
                                <li><a class="dropdown-item" href="busquedap.jsp"><%=rs.getString(2)%></a></li>
                                    <%                                                      }
                                        } catch (Exception e) {
                                            out.print("error mysql" + e);
                                        } finally {
                                            try {
                                                con.close();
                                            } catch (SQLException e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                            }
                                        }
                                    %>                 
                            </ul>
                        </li>
                    </ul>
                    <form action="busqueda.jsp" class="was-validated d-flex" method="POST">
                        <input class="form-control me-2" id="buscarpreg" name="buscarpreg" type="search" placeholder="Escribe para buscar" aria-label="Search">
                        <button class="btn btn-outline-success" name="enviarbus" type="submit">Buscar</button>
                    </form>
                </div>
            </div>
        </nav>
        <!--Header-->

        <br>
        <br>
        <br>
        
        <%

            try {
                Class.forName("com.mysql.jdbc.Driver");

                con = DriverManager.getConnection(url, "root", "root");
                float number = 0;
                st = con.createStatement();
                rs = st.executeQuery("SELECT IdPreguntas,Pregunta,Id_Categoria,Id_Usuario,Descripcion,Imagen,Editada,"
                        + "preguntas.Fecha_Creacion,preguntas.Activo,usuarios.Username,usuarios.Imagen_Perfil"
                        + " FROM preguntas,usuarios WHERE preguntas.Activo = true and preguntas.Id_Usuario = usuarios.IdUsuarios"
                        + " and if('"+busqueda+"' Is null,1, Pregunta like concat('%','"+busqueda+"','%'))");

                while (rs.next()) {
                    Blob ImagenP = null;
                    Blob ImagenP2 = null;
                    String imgDataBase64 = "";
                    String imgDataBase642 = "";
                    number++;
                    String IdPregunta = rs.getString(1);
                    String Pregunta = rs.getString(2);
                    String IdCategoria = rs.getString(3);
                    String Id_Usuario = rs.getString(4);
                    String Descripcion = rs.getString(5);
                    String Imagen = rs.getString(6);
                    Boolean Editada = rs.getBoolean(7);
                    String Fecha = rs.getString(8);
                    Boolean Activo = rs.getBoolean(9);
                    String Username = rs.getString(10);
                    String UserFoto = rs.getString(11);
                    ImagenP = rs.getBlob(11);
                    ImagenP2 = rs.getBlob(6);
                    byte[] imgData = null;
                    byte[] imgData2 = null;
                    imgData = ImagenP.getBytes(1, (int) ImagenP.length());
                    imgData2 = ImagenP2.getBytes(1, (int) ImagenP2.length());
                    imgDataBase64 = new String(Base64.getEncoder().encode(imgData));
                    imgDataBase642 = new String(Base64.getEncoder().encode(imgData2));
        %>
        <div class="row"style="align-text:center;">
            <div class="col-sm-4"></div>
            <div class="col-sm-6">
                <div class="card w-100" style="padding-left: 10px; max-width: 540px;">

                    <div class="row g-0">
                        <div class="card-header"><p> por <img src="data:image/gif;base64,<%= imgDataBase64%>" alt="" style="width:40px; border-radius:25px;padding-left:5px;padding-right:5px;"><%if (UserLogged.compareTo(Id_Usuario) == 0) {%><a href="perfilpropio.jsp" <%} else {%><a href="perfilajeno.jsp?idu=<%=Id_Usuario%>" <%}%>"> <%=Username%></a> </p></div>
                        <div class="col-md-4">
                            <img src="data:image/gif;base64,<%= imgDataBase642%>" style="width: 100%; "alt="...">
                        </div>
                        <div class="col-md-8">

                            <div class="card-body">
                                <h5 class="card-title"><%=Pregunta%></h5>
                                <p class="card-text"><%=Descripcion%></p>
                                <a href="pregunta.jsp?idp=<%=IdPregunta%>" class="btn btn-primary">Ir a la pregunta</a>
                                <p class="card-text"><small class="text-muted"><% if (Editada == true) {%>Editada<%}%></small></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>                    

        <%
                }

            } catch (Exception e) {
                out.print("error mysql 1" + e);
            } finally {
                try {
                    con.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        %>


        <br>
        <br>


        <!--Footer-->
        <footer class="page-footer bg-dark">
            <div style="background-color:#ea4f02">
                <div class="container">
                    <div class="row py-4 d-flex align-items-center">
                        <div class="col-md-12 text-center">
                            <a href="#"><i class="fab fa-facebook-square text-white mr-3"></i></a>
                            <a href="#"><i class="fab fa-twitter-square text-white mr-3"></i></a>
                            <a href="#"><i class="fab fa-instagram-square text-white"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container text-center  text-light text-md-left mt-5">
                <div class="row">
                    <div class="col-md-3 mx-auto mb-4">
                        <h6 class="text-uppercase font-weight-bold">Desarrolladores:</h6>
                        <hr class="bg-success mb-4 mt-0 d-inline-block mx-auto" style="width: 125px; height: 2px;">
                        <p class="mt-1">Fernando Moncayo Marquez</p>
                        <a class="mt-1 text-white" href="#" >fermoncam506@gmail.com</a>
                        <p class="mt-1">Alma Cecilia Villareal Franco</p>
                        <a class="mt-1 text-white" href="#">ce.cy99@live.com</a>
                    </div>

                    <div class="col-md-2 mx-auto mb-4">
                        <h6 class="text-uppercase font-weight-bold">Categorias:</h6>
                        <hr class="bg-success mb-4 mt-0 d-inline-block mx-auto" style="width: 85px; height: 2px;">

                        <ul class="list-unstyled ">
                            <%                            try {
                                    Class.forName("com.mysql.jdbc.Driver");

                                    con = DriverManager.getConnection(url, "root", "root");
                                    st = con.createStatement();
                                    rs = st.executeQuery("SELECT * FROM categorias");

                                    while (rs.next()) {

                            %>
                            <li class="my-2" ><a href="busqueda.jsp?id=<%=rs.getInt(1)%>" class="text-white"><%=  rs.getString(2)%></a></li>
                                <%
                                        }
                                    } catch (Exception e) {
                                        out.print("error mysql 2" + e);
                                    } finally {
                                        try {
                                            con.close();
                                        } catch (SQLException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                    }
                                %>
                        </ul>

                    </div>

                    <div class="col-md-2 mx-auto mb-4">
                        <h6 class="text-uppercase font-weight-bold">Repositorio:</h6>
                        <hr class="bg-success mb-4 mt-0 d-inline-block mx-auto" style="width: 110px; height: 2px">
                        <p></p>

                        <a class="mt-1 text-white" href="https://gitlab.com/Fercho_SMT/proyecto_programacion_web"  target = "_blank">Repositorio del proyecto</a>
                    </div>

                    <div class="col-md-2 mx-auto mb-4">
                        <h6 class="text-uppercase font-weight-bold">Ask & Cook:</h6>
                        <hr class="bg-success mb-4 mt-0 d-inline-block mx-auto" style="width: 85px; height: 2px;">
                        <ul class="list-unstyled ">
                            <li class="my-2" ><i class="fas fa-home mr-3"> Monterrey, Nuevo Leon</i></li>
                            <li class="my-2" ><i class="fas fa-envelope mr-3"> askandcook@gmail.com</i></li>
                            <li class="my-2" ><i class="fas fa-phone  mr-3"> 8112298194</i></li>
                        </ul>
                    </div>


                </div>
            </div>

            <div class="footer-copyright text-center py-3 text-white bg-black">
                <p>&copy; Copyright
                    <a href="principal.jsp" class="text-white">askandcook.com</a>
                <p >Diseñado por Ask&Cook</p>
                </p>
            </div>
        </footer>
        <!--Footer-->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
        <!-- MDB -->
        <script
            type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"
        ></script>


    </body>
</html>

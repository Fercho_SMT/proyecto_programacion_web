CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Preguntas`(
OPC 				char(1),
IdPreguntas			INT,
Pregunta 			VARCHAR(100),
Id_Categoria 		INT,
Usuario 			INT,
Descripcion 		VARCHAR(200),
Imagen 				mediumblob,
Editada 			TINYINT(1),
Fecha_Creacion 		DATE,
Activo 				TINYINT(1)
)
BEGIN
declare IdCate int;
declare IdUser int;

set IdCate = (SELECT Categorias.IdCategorias FROM Categorias WHERE Categorias.IdCategorias = Id_Categoria),
	IdUser = (SELECT Usuarios.IdUsuarios FROM Usuarios WHERE Usuarios.IdUsuarios = Usuario);

IF OPC = 'I' then	#insert
	begin
		insert into Preguntas (Preguntas.Pregunta, Preguntas.Id_Categoria, Preguntas.Id_Usuario, Preguntas.Descripcion, 
        Preguntas.Imagen, Preguntas.Editada, Preguntas.Fecha_Creacion, Preguntas.Activo) 
		values (Pregunta, IdCate, IdUser, Descripcion, Imagen, 0, now(), 1);    
	end;
elseif OPC = 'U' then	#update
	begin
		update Preguntas 
		set Preguntas.Pregunta = Pregunta, 
			Preguntas.Id_Categoria = IdCate, 
			Preguntas.Descripcion = Descripcion, 
			Preguntas.Imagen = Imagen, 
			Preguntas.Editada = 1
            where Preguntas.IdPreguntas = IdPreguntas;
	end;
elseif OPC = 'B' then	#baja_logica
	begin
		update Preguntas 
		set Preguntas.Activo = 0
			where Preguntas.IdPreguntas = IdPreguntas;
            update respuestas set Respuestas.Activo = 0 Where Respuestas.Id_Pregunta = IdPreguntas;
    end;
end if;
END
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_RespuestasValoradas`(
OPC 					char(1),
IdRespuestas_valoradas	INT,
Respuesta 				INT,
Usuario 				INT,
Util 					TINYINT(1),
No_Util 				TINYINT(1)
)
BEGIN
declare IdUSer int;
declare IdAnswer int;

set IdUser = (SELECT Usuarios.IdUsuarios FROM Usuarios WHERE Usuario.Username = Usuario), 
    IdAnswer = (SELECT Respuestas.IdRespuestas FROM Respuestas WHERE Respuestas.Respuesta = Respuesta);

IF OPC = 'I' then	#insert
	begin
		insert into Respuestas_Valoradas (Respuestas_Valoradas.Id_Respuesta, Respuestas_Valoradas.Id_Usuario, 
        Respuestas_Valoradas.Util, Respuestas_Valoradas.No_Util) 
		values (IdAnswer, IdUser, Util, No_Util);    
	end;
elseif OPC = 'D' then	#delete
	begin
		delete
        from Respuestas_Valoradas 
		where Respuestas_Valoradas.IdRespuestas_valoradas = IdRespuestas_valoradas;
    end;
end if;
END
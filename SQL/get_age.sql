USE `db_ask_n_cook`;

DELIMITER $$
CREATE FUNCTION `getage`(Fecha_nacimiento date)
RETURNS INTEGER
DETERMINISTIC
BEGIN
declare fecha_hoy date;
declare fecha_nac date;
declare year_hoy int;
declare year_nac int;
declare mes_hoy int;
declare mes_nac int;
declare dia_hoy int;
declare dia_nac int;

declare edad int;

set fecha_hoy = current_date();
set fecha_nac = Fecha_nacimiento;
set year_hoy = year(current_date());
set year_nac = year(Fecha_nacimiento);
set mes_hoy = month(current_date());
set mes_nac = month(Fecha_nacimiento);
set dia_hoy = day(current_date());
set dia_nac = day(Fecha_nacimiento);

if mes_hoy < mes_nac then
	begin
		set edad = year_hoy - year_nac - 1;
	end;
elseif mes_hoy > mes_nac then
	begin
		set edad = year_hoy - year_nac;
	end;
elseif mes_hoy = mes_nac then
	begin
		if dia_hoy < dia_nac then
			begin
				set edad = year_hoy - year_nac -1;
            end;
		elseif dia_hoy >= dia_nac then
			begin
				set edad = year_hoy - year_nac -1;
            end;
		end if;
	end;
end if;

RETURN edad;
END$$
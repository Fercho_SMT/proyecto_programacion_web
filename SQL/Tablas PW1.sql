DROP DATABASE IF EXISTS DB_ask_n_cook;
CREATE DATABASE IF NOT EXISTS DB_ask_n_cook;

USE DB_ask_n_cook;

DROP TABLE IF EXISTS Preguntas_Favoritas;
DROP TABLE IF EXISTS Respuestas_Valoradas;
DROP TABLE IF EXISTS Preguntas_Valoradas;
DROP TABLE IF EXISTS Respuestas;
DROP TABLE IF EXISTS Preguntas;
DROP TABLE IF EXISTS Categorias;
DROP TABLE IF EXISTS Usuarios;

# Usuarios
CREATE TABLE IF NOT EXISTS Usuarios (
IdUsuarios			INT NOT NULL AUTO_INCREMENT,
Nombre 				VARCHAR(30) NOT NULL,
Apellido_P 			VARCHAR(30) NULL,
Apellido_M 			VARCHAR(30) NULL,
Fecha_Nacimiento 	DATE NULL,
Imagen_Perfil 		mediumblob NULL,
Correo 				VARCHAR(50) NOT NULL,
Username 			VARCHAR(20) NOT NULL,
Contraseña 			VARCHAR(45) NOT NULL,
Fecha_Creacion 		DATE NOT NULL,
Activo 				TINYINT(1) NULL DEFAULT 1,
CONSTRAINT PK_Usuarios PRIMARY KEY (IdUsuarios)
);

#Categorias
CREATE TABLE IF NOT EXISTS Categorias (
IdCategorias 		INT NOT NULL AUTO_INCREMENT,
Nombre_Categoria 	VARCHAR(45) NOT NULL,
CONSTRAINT PK_Categorias PRIMARY KEY (IdCategorias)
);

#Preguntas
CREATE TABLE IF NOT EXISTS Preguntas (
IdPreguntas			INT NOT NULL AUTO_INCREMENT,
Pregunta 			VARCHAR(100) NOT NULL,
Id_Categoria 		INT NULL,
Id_Usuario 			INT NULL,
Descripcion 		VARCHAR(200) NULL,
Imagen 				mediumblob NULL,
Editada 			TINYINT(1) NULL,
Fecha_Creacion 		DATE,
Activo 				TINYINT(1) NULL DEFAULT 1,
CONSTRAINT PK_Preguntas PRIMARY KEY (IdPreguntas),
CONSTRAINT FK_Preguntas_Categorias FOREIGN KEY (Id_Categoria) REFERENCES Categorias (IdCategorias),
CONSTRAINT FK_Preguntas_Usuario FOREIGN KEY (Id_Usuario) REFERENCES Usuarios (IdUsuarios)
);

#Respuestas
CREATE TABLE IF NOT EXISTS Respuestas (
IdRespuestas 		INT NOT NULL AUTO_INCREMENT,
Id_Usuario 			INT NULL,
Respuesta 			VARCHAR(200) NOT NULL,
Imagen 				mediumblob NULL,
Editada 			TINYINT(1) NULL,
Activo 				TINYINT(1) NULL DEFAULT 1,
Id_Pregunta 		INT NULL,
Correcta			TINYINT(1) NULL,
CONSTRAINT PK_Respuestas PRIMARY KEY (IdRespuestas),
CONSTRAINT FK_Respuestas_Usuario FOREIGN KEY (Id_Usuario) REFERENCES Usuarios (IdUsuarios),
CONSTRAINT FK_Respuestas_Preguntas FOREIGN KEY (Id_Pregunta) REFERENCES Preguntas (IdPreguntas)
);

#Preguntas Valoradas
CREATE TABLE IF NOT EXISTS Preguntas_Valoradas (
IdPreguntas_valoradas	INT NOT NULL AUTO_INCREMENT,
Id_Pregunta 			INT NULL,
Id_Usuario 				INT NULL,
Util 					TINYINT(1) NULL,
No_Util 				TINYINT(1) NULL,
CONSTRAINT PK_PreguntasValoradas PRIMARY KEY (IdPreguntas_valoradas),
CONSTRAINT FK_PV_Preguntas FOREIGN KEY (Id_Pregunta)REFERENCES Preguntas (IdPreguntas),
CONSTRAINT FK_PV_Usuarios FOREIGN KEY (Id_Usuario)REFERENCES Usuarios (IdUsuarios)
);

#Respuestas Valoradas
CREATE TABLE IF NOT EXISTS Respuestas_Valoradas (
IdRespuestas_valoradas 	INT NOT NULL AUTO_INCREMENT,
Id_Respuesta 			INT NULL,
Id_Usuario 				INT NULL,
Util 					TINYINT(1) NULL,
No_Util 				TINYINT(1) NULL,
CONSTRAINT PK_RespuestasValoradas PRIMARY KEY (IdRespuestas_valoradas),
CONSTRAINT FK_RV_Respuestas FOREIGN KEY (Id_Respuesta)REFERENCES Respuestas (IdRespuestas),
CONSTRAINT FK_RV_Usuarios FOREIGN KEY (Id_Usuario)REFERENCES Usuarios (IdUsuarios)
);

#Preguntas Favoritas
CREATE TABLE IF NOT EXISTS Preguntas_Favoritas (
IdPreguntas_Favoritas 	INT NOT NULL AUTO_INCREMENT,
Id_Pregunta 			INT NULL,
Id_Usuario 				INT NULL,
Favorito 				TINYINT(1) NULL,
CONSTRAINT PK_PreguntasFavoritas PRIMARY KEY (IdPreguntas_Favoritas),
CONSTRAINT FK_PF_Preguntas FOREIGN KEY (Id_Pregunta)REFERENCES Preguntas (IdPreguntas),
CONSTRAINT FK_PF_Usuario FOREIGN KEY (Id_Usuario)REFERENCES Usuarios (IdUsuarios)
);

SHOW TABLES FROM DB_ask_n_cook;

SELECT * from categorias;


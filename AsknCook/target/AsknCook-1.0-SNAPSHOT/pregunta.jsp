<%-- 
    Document   : pregunta
    Created on : 25/05/2021, 09:00:07 PM
    Author     : Usuario
--%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.util.Base64"%>
<%@page import="java.sql.*"%>
<%@page import="clases.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Registro</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

        <link rel="shortcut icon" type="image/x-icon" href="./images/askandcook.png">
        <link rel="stylesheet" type="text/css" href="css/registro.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <!--Footer-->
        <!-- Font Awesome -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css"
            rel="stylesheet"
            />
        <!--Footer-->

        <style>
            .bg{
                background-image: url(./images/cooking.jpg);
                background-position: center center;
                background-size: 600px 600px;
                border-radius: 10px 0px 0px 10px;
            }
        </style>

    </head>
    <body>
        <%

            Connection con = null;
            Statement st = null;
            ResultSet rs = null;
            ResultSet rsr = null;
            ResultSet rsr2 = null;
            ResultSet rsr3 = null;
            String idPregunta = "";
            if (request.getParameter("idp") != null) {
                idPregunta = request.getParameter("idp");
            } else {
                if (request.getAttribute("idp") != null) {
                    idPregunta = (String) request.getAttribute("idp");
                }
            }

            int UserLoggedI;
            String UserLogged = "0";
            User u = (User) session.getAttribute("User");
            if (u != null) {
                UserLoggedI = u.getId();
                UserLogged = Integer.toString(UserLoggedI);

            } else {
                UserLoggedI = 0;
            }

        %>
        <!--Header-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="position: fixed; width: 100%; top:0px; z-index: 1000;">
            <div class="container-fluid">
                <a class="navbar-brand" href="principal.jsp"><img src="./images/askandcook.png" width="50" alt="Error de carga"></a>
                <button class="navbar-toggler " type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon bg-dark"></span>
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="principal.jsp">Inicio</a>

                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" <%if (UserLoggedI == 0) {%> href="inicio.jsp"  <%} else {%> href="perfilpropio.jsp?idu=<%=UserLoggedI%>"<%}%>>Perfil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="busquedap.jsp">Busqueda Avanzada</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Categorias
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <%
                                    String servername = "localhost";
                                    String port = "3306";
                                    String databaseName = "db_ask_n_cook";
                                    String url = "jdbc:mysql://" + servername + ":" + port + "/" + databaseName;
                                    try {
                                        Class.forName("com.mysql.jdbc.Driver");

                                        con = DriverManager.getConnection(url, "root", "root");
                                        st = con.createStatement();
                                        rs = st.executeQuery("SELECT * FROM categorias");

                                        while (rs.next()) {
                                %>
                                <li><a class="dropdown-item" href="busquedap.jsp"><%=rs.getString(2)%></a></li>
                                    <%                                                      }
                                        } catch (Exception e) {
                                            out.print("error mysql 1" + e);
                                        } finally {
                                            try {
                                                con.close();
                                            } catch (SQLException e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                            }
                                        }
                                    %>                 
                            </ul>
                        </li>
                    </ul>
                    <form action="busqueda.jsp" class="was-validated d-flex" method="POST">
                        <input class="form-control me-2" id="buscarpreg" name="buscarpreg" type="search" placeholder="Escribe para buscar" aria-label="Search">
                        <button class="btn btn-outline-success" name="enviarbus" type="submit">Buscar</button>
                    </form>
                </div>
            </div>
        </nav>

        <!--Header-->

        <br>
        <div style="height: 50px;"></div>
        <br>

        <%
            String Pregunta = "";
            String Categoria = "";
            String IdUser = "";
            String Descripcion = "";
            String Imagen = "";
            Boolean Editada = false;
            String Fecha = "";
            Boolean Activo = true;
            int numeroUtil = 0;
            int numeroNoUtil = 0;
            int numeroFav = 0;
            String FotoU = "";
            String UserDB = "";
            String nulo = null;
            Blob ImagenP = null;
            String imgDataBase64 = "";
            Blob ImagenP4 = null;
            String imgDataBase644 = "";

            try {

                Class.forName("com.mysql.jdbc.Driver");

                con = DriverManager.getConnection(url, "root", "root");

                try {
                    st = con.createStatement();
                    rs = st.executeQuery("SELECT * FROM preguntas WHERE IdPreguntas =" + idPregunta);
                    if (rs.next()) {
                        Pregunta = rs.getString(2);
                        Categoria = rs.getString(3);
                        IdUser = rs.getString(4);
                        Descripcion = rs.getString(5);
                        Imagen = rs.getString(6);
                        Editada = rs.getBoolean(7);
                        Fecha = rs.getString(8);
                        Activo = rs.getBoolean(7);

                        ImagenP4 = rs.getBlob(6);
                        byte[] imgData2 = null;
                        imgData2 = ImagenP4.getBytes(1, (int) ImagenP4.length());
                        imgDataBase644 = new String(Base64.getEncoder().encode(imgData2));
                    }
                } catch (Exception e) {
                    out.print("error mysql 2" + e);
                } finally {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                try {
                    con = DriverManager.getConnection(url, "root", "root");
                    st = con.createStatement();
                    rsr2 = st.executeQuery("SELECT Util,No_Util FROM preguntas_valoradas WHERE Id_Pregunta = " + idPregunta);
                    numeroUtil = 0;
                    numeroNoUtil = 0;
                    while (rsr2.next()) {
                        if (rsr2.getBoolean(1) == true) {
                            numeroUtil++;
                        }
                        if (rsr2.getBoolean(2) == true) {
                            numeroNoUtil++;
                        }
                    }
                } catch (Exception e) {
                    out.print("error mysql 3.1" + e);
                } finally {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                try {
                    con = DriverManager.getConnection(url, "root", "root");
                    st = con.createStatement();
                    rsr2 = st.executeQuery("SELECT Favorito FROM preguntas_favoritas WHERE Id_Pregunta = " + idPregunta);
                    numeroFav = 0;
                    while (rsr2.next()) {
                        if (rsr2.getBoolean(1) == true) {
                            numeroFav++;
                        }

                    }
                } catch (Exception e) {
                    out.print("error mysql 4" + e);
                } finally {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                try {
                    con = DriverManager.getConnection(url, "root", "root");
                    st = con.createStatement();
                    rs = st.executeQuery("SELECT * FROM usuarios WHERE IdUsuarios =" + IdUser);
                    if (rs.next()) {
                        FotoU = rs.getString(6);
                        UserDB = rs.getString(8);
                        ImagenP = rs.getBlob(6);

                        byte[] imgData = null;

                        imgData = ImagenP.getBytes(1, (int) ImagenP.length());

                        imgDataBase64 = new String(Base64.getEncoder().encode(imgData));
                    }
                } catch (Exception e) {
                    out.print("error mysql 5" + e);
                } finally {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {
                out.print("error mysql 6" + e);
            } finally {
                try {
                    con.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        %>

        <!--Pregunta-->
        <div class="container">
            <div class="row">
                <!--Titulo-->
                <div class="row">
                    <h1 class="mt-4" ><%=Pregunta%> 
                    </h1>

                </div>
                <%
                    if (UserLogged.compareTo(IdUser) == 0) {
                %>
                <div class="user-buttons">
                    <button class="btn btn-success btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#ModalEditP" >Editar  <i class="fas fa-user-edit"></i></button>
                    <button class="btn btn-danger btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#ModalDeleteP" >Borrar  <i class="fas fa-trash-alt"></i></button>
                </div>
                <%
                    }
                %>      
                <!--Modal Edit Pregunta-->
                <div class="modal fade" id="ModalEditP" tabindex="-1" aria-labelledby="ModalEditLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Editar Pregunta</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <form action="pregunta.jsp?idp=<%=idPregunta%>" class="was-validated" method="POST">
                                <div class="modal-body">


                                    <legend>Llena los campos para editar tu pregunta</legend>
                                    <div class="mb-3">
                                        <label for="pregunta" class="form-label">Pregunta</label>
                                        <input id="preguntaEM" name="preguntaEM" class="form-control" type="text" aria-label="default input example" value="<%=Pregunta%>" required>
                                    </div>
                                    <div class="mb-3">
                                        <label for="categoria" class="form-label">Categoria: </label>
                                        <select class="form-select" aria-label="Default select example" id="categoriaEM" name="categoriaEM" required>
                                            <%
                                                try {
                                                    Class.forName("com.mysql.jdbc.Driver");

                                                    con = DriverManager.getConnection(url, "root", "root");
                                                    st = con.createStatement();
                                                    rs = st.executeQuery("SELECT * FROM categorias");
                                                    while (rs.next()) {
                                            %>
                                            <%
                                                if (Categoria.compareTo(rs.getString(1)) == 0) {
                                            %>
                                            <option value="<%=rs.getString(1)%>" selected><%=rs.getString(2)%></option>
                                            <%
                                            } else {

                                            %>      
                                            <option value="<%=rs.getString(1)%>"><%=rs.getString(2)%></option>
                                            <%
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    out.print("error mysql 4" + e);
                                                } finally {
                                                    try {
                                                        con.close();
                                                    } catch (SQLException e) {
                                                        // TODO Auto-generated catch block
                                                        e.printStackTrace();
                                                    }
                                                }
                                            %>  

                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="descripcion" class="form-label">Descripcion</label>
                                        <textarea class="form-control" id="descripcionEM" name ="descripcionEM"  rows="3"><%=Descripcion%></textarea>
                                    </div>
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Seleccione una imagen para su pregunta</label>
                                        <input class="form-control" type="file" id="fotoEM" value="" name="fotoEM">
                                    </div>
                                    <button type="submit" name="enviarpEM" class="btn btn-primary">Guardar cambios</button>
                                    <input  type="hidden" id="idp" name="idp" value="<%=idPregunta%>">

                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <%
                    if (request.getParameter("enviarpEM") != null) {
                        try {
                            String PreguntaE = request.getParameter("preguntaEM");
                            String CategoriaE = request.getParameter("categoriaEM");
                            String DescripcionE = request.getParameter("descripcionEM");
                            String ImagenE = request.getParameter("fotoEM");
                            Class.forName("com.mysql.jdbc.Driver");

                            con = DriverManager.getConnection(url, "root", "root");
                            st = con.createStatement();
                            //out.print("CALL sp_Preguntas('U'," + idPregunta + ",'"+ PreguntaE + "'," + CategoriaE + "," + IdUser + ",'" + DescripcionE + "','" + ImagenE + "',1,'" + Fecha + "',1)");
                            st.executeUpdate("CALL sp_Preguntas('U'," + idPregunta + ",'" + PreguntaE + "'," + CategoriaE + "," + IdUser + ",'" + DescripcionE + "','" + ImagenE + "',1,'" + Fecha + "',1)");
                            //request.getRequestDispatcher("pregunta.jsp?idp=" + idPregunta).forward(request,response);

                        } catch (Exception e) {
                            out.print("error mysql 3.2.2" + e);
                        } finally {
                            try {
                                con.close();
                            } catch (SQLException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }
                %>
                <!--Modal Edit Pregunta-->

                <!--Modal Delete Pregunta-->
                <div class="modal fade" id="ModalDeleteP" tabindex="-1" aria-labelledby="ModalDeleteLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">¿Quieres borrar tu pregunta?</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <form action="pregunta.jsp?idp=<%=idPregunta%>" class="was-validated" method="GET">
                                <br>

                                <div style=" text-align: center;">
                                    <button type="submit" name="enviarpDM" class="btn btn-primary"  >Borrar</button>
                                    <input  type="hidden" id="idp" name="idp" value="<%=idPregunta%>">
                                </div>
                                <br>
                                <br>
                            </form>

                        </div>
                    </div>

                </div> 
                <%
                    if (request.getParameter("enviarpDM") != null) {
                        try {

                            Class.forName("com.mysql.jdbc.Driver");

                            con = DriverManager.getConnection(url, "root", "root");
                            st = con.createStatement();

                            st.executeUpdate("CALL sp_Preguntas('B'," + idPregunta + ",'',0,0,'','',0,'2021-03-06',0)");
                            request.getRequestDispatcher("principal.jsp").forward(request, response);

                        } catch (Exception e) {
                            out.print("error 3.3" + e);
                        } finally {
                            try {
                                con.close();
                            } catch (SQLException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }

                %>
                <!--Modal Delete Pregunta-->

                <!--Autor + Foto de perfil-->
                <p class="lead"> por <img src="data:image/gif;base64,<%= imgDataBase64%>" alt="" style="width:50px; border-radius:25px;padding-left:5px;padding-right:5px;"><%if (UserLogged.compareTo(IdUser) == 0) {%><a href="perfilpropio.jsp"><%} else {%><a href="perfilajeno.jsp?idu=<%=IdUser%>"><%}%><%=UserDB%></a> 

                </p>

                <hr>
                <!--Fecha de la publicacion-->
                <p class="fecha_publicacion"> <%=Fecha%></p>
                <hr>
                <!--Imagen de la publicacion-->
                <img class="img-fluid rounded" src="data:image/gif;base64,<%= imgDataBase644%>" alt="">
                <br>
                <br>
                <!--Contenido del post-->
                <p class="contenido_post"><%=Descripcion%></p>
                <!--Iconos de valoracion-->
                <div class="user-buttons">
                    <form action="pregunta.jsp?idp=<%=idPregunta%>" class="was-validated" method="GET">
                        <input  type="hidden" id="idp" name="idp" value="<%=idPregunta%>">
                        <button class="btn btn-primary btn-sm" type="submit" name="UtilP" ><i class="fas fa-thumbs-up"></i><%=numeroUtil%></button>
                        <button class="btn btn-primary btn-sm" type="submit" name="NoUtilP" ><i class="fas fa-thumbs-down"></i><%=numeroNoUtil%></button>
                        <button class="btn btn-primary btn-sm" type="submit" name="FavP" ><i class="fas fa-star"></i><%=numeroFav%></button> 
                    </form>
                    <br>
                    <%


                        if (request.getParameter("UtilP") != null && UserLoggedI != 0) {
                            if (UserLoggedI != 0) {
                                Boolean condition;
                                try {
                                    con = DriverManager.getConnection(url, "root", "root");
                                    st = con.createStatement();
                                    rs = st.executeQuery("SELECT * FROM preguntas_valoradas Where Id_Pregunta =" + idPregunta + " and Id_Usuario=" + UserLogged);
                                    if (rs.next()) {
                                        Boolean isUtil = rs.getBoolean(3);
                                        if (isUtil == true) {
                                            st = con.createStatement();
                                            st.executeUpdate("DELETE FROM preguntas_valoradas WHERE Id_Pregunta= " + idPregunta + " and Id_Usuario=" + UserLogged);
                                        } else {
                                            st = con.createStatement();
                                            st.executeUpdate("Update preguntas_valoradas SET Util=1,No_Util=0 WHERE Id_Pregunta= " + idPregunta + " and Id_Usuario=" + UserLogged);
                                        }
                                    } else {
                                        st = con.createStatement();
                                        st.executeUpdate("INSERT into preguntas_valoradas(Id_Pregunta,Id_Usuario,Util,No_Util) values(" + idPregunta + "," + UserLogged + ",1,0)");
                                    }

                                } catch (Exception e) {
                                    out.print("error mysql preguntas util" + e);
                                } finally {
                                    try {
                                        con.close();
                                    } catch (SQLException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }

                    %>
                    <%                        if (request.getParameter("NoUtilP") != null && UserLoggedI != 0) {
                            if (UserLoggedI != 0) {
                                Boolean condition;
                                try {
                                    con = DriverManager.getConnection(url, "root", "root");
                                    st = con.createStatement();
                                    rs = st.executeQuery("SELECT * FROM preguntas_valoradas Where Id_Pregunta =" + idPregunta + " and Id_Usuario=" + UserLogged);
                                    if (rs.next()) {
                                        Boolean isNoUtil = rs.getBoolean(4);
                                        if (isNoUtil == true) {
                                            st = con.createStatement();
                                            st.executeUpdate("DELETE FROM preguntas_valoradas WHERE Id_Pregunta= " + idPregunta + " and Id_Usuario=" + UserLogged);

                                        } else {
                                            st = con.createStatement();
                                            st.executeUpdate("Update preguntas_valoradas SET Util=0,No_Util=1 WHERE Id_Pregunta= " + idPregunta + " and Id_Usuario=" + UserLogged);

                                        }
                                    } else {
                                        st = con.createStatement();
                                        st.executeUpdate("INSERT into preguntas_valoradas(Id_Pregunta,Id_Usuario,Util,No_Util) values(" + idPregunta + "," + UserLogged + ",0,1)");
                                    }

                                } catch (Exception e) {
                                    out.print("error mysql preguntas no utiles" + e);
                                } finally {
                                    try {
                                        con.close();
                                    } catch (SQLException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }

                    %>
                    <%                        if (request.getParameter("FavP") != null && UserLoggedI != 0) {
                            if (UserLoggedI != 0) {
                                Boolean condition;
                                try {
                                    con = DriverManager.getConnection(url, "root", "root");
                                    st = con.createStatement();
                                    rs = st.executeQuery("SELECT * FROM preguntas_favoritas Where Id_Pregunta =" + idPregunta + " and Id_Usuario=" + UserLogged);
                                    if (rs.next()) {
                                        st = con.createStatement();
                                        st.executeUpdate("DELETE FROM preguntas_favoritas WHERE Id_Pregunta= " + idPregunta + " and Id_Usuario=" + UserLogged);

                                    } else {
                                        st = con.createStatement();
                                        st.executeUpdate("INSERT into preguntas_favoritas(Id_Pregunta,Id_Usuario,Favorito) values(" + idPregunta + "," + UserLogged + ",1)");
                                    }
                                } catch (Exception e) {
                                    out.print("error mysql preguntas favoritos" + e);
                                } finally {
                                    try {
                                        con.close();
                                    } catch (SQLException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }


                    %>
                </div>
                <br>
                <hr>
                <%                    String respuestaF = request.getParameter("respuestaF");
                    String fotoRF = request.getParameter("fotoRF");
                %>

                <!--Escribe Respuesta-->
                <div class="card my-4">
                    <h5 class="card-header">Deja un comentario:</h5>

                    <div class="card-body">
                        <form action="pregunta.jsp?idp=<%=idPregunta%>" class="was-validated" method="GET">
                            <div class="form-group">
                                <textarea id="respuestaF" name="respuestaF" value="<%=respuestaF%>" class="form-control" rows="3" required></textarea><br>
                            </div>
                            <!--Imagen-->
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Seleccione una imagen para su pregunta</label>
                                <input class="form-control" type="file" id="fotoRF" name="fotoRF">
                            </div>              
                            <!--Imagen-->
                            <button type="submit" name="enviarR" value="<%=nulo%>" class="btn btn-primary">Publicar</button>
                            <input  type="hidden" id="idp" name="idp" value="<%=idPregunta%>">
                        </form>
                    </div>
                </div>
                <hr>
                <%

                    if (request.getParameter("enviarR") != null && UserLoggedI != 0) {

                        try {

                            Class.forName("com.mysql.jdbc.Driver");

                            con = DriverManager.getConnection(url, "root", "root");
                            st = con.createStatement();
                            out.print("respuestaF " + respuestaF);
                            st.executeUpdate("CALL sp_Respuestas('I',0," + UserLoggedI + ",'" + respuestaF + "','" + fotoRF + "',0,1," + idPregunta + ",0 )");
                            //  request.getRequestDispatcher("pregunta.jsp?idp="+idPregunta+"&enviarR="+ null).forward(request, response);

                        } catch (Exception e) {
                            out.print("error mysql 3.4" + e);
                        } finally {
                            try {
                                con.close();
                            } catch (SQLException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }

                    }

                %>


                <h1 class="mt-4">Comentarios</h1>
                <!--Escribe Respuesta-->
                <hr>
                <!--Respuestas-->
                <%                    String IdRespuesta = "";
                    String IdUsuario = "";
                    String Respuesta = "";
                    String ImagenRC = "";
                    Boolean EditadaRC = false;
                    Boolean ActivoRC = true;
                    Boolean Correcta = false;
                    Blob ImagenP2 = null;
                    String imgDataBase642 = "";
                    Blob ImagenP5 = null;
                    String imgDataBase645 = "";

                    int numeroUtilRC = 0;
                    int numeroNoUtilRC = 0;

                    String FotoURC = "";
                    String UserDBRC = "";

                    try {

                        Class.forName("com.mysql.jdbc.Driver");

                        con = DriverManager.getConnection(url, "root", "root");
                        st = con.createStatement();
                        rs = st.executeQuery("SELECT * FROM respuestas WHERE Activo= 1 and Correcta = 1 and Id_Pregunta =" + idPregunta);

                        while (rs.next()) {
                            IdRespuesta = rs.getString(1);
                            IdUsuario = rs.getString(2);
                            Respuesta = rs.getString(3);
                            ImagenRC = rs.getString(4);
                            EditadaRC = rs.getBoolean(5);
                            ActivoRC = rs.getBoolean(6);
                            Correcta = rs.getBoolean(8);
                            st = con.createStatement();
                            rsr2 = st.executeQuery("SELECT Util,No_Util FROM respuestas_valoradas WHERE Id_Respuesta = " + IdRespuesta);
                            ImagenP5 = rs.getBlob(4);

                            byte[] imgData5 = null;

                            imgData5 = ImagenP5.getBytes(1, (int) ImagenP5.length());

                            imgDataBase645 = new String(Base64.getEncoder().encode(imgData5));

                            numeroUtilRC = 0;
                            numeroNoUtilRC = 0;
                            while (rsr2.next()) {
                                if (rsr2.getBoolean(1) == true) {
                                    numeroUtilRC++;
                                }
                                if (rsr2.getBoolean(2) == true) {
                                    numeroNoUtilRC++;
                                }
                            }

                            st = con.createStatement();
                            rsr = st.executeQuery("SELECT * FROM usuarios WHERE IdUsuarios =" + IdUsuario);
                            if (rsr.next()) {
                                FotoURC = rsr.getString(6);
                                UserDBRC = rsr.getString(8);
                                ImagenP2 = rsr.getBlob(6);

                                byte[] imgData2 = null;

                                imgData2 = ImagenP2.getBytes(1, (int) ImagenP2.length());

                                imgDataBase642 = new String(Base64.getEncoder().encode(imgData2));
                            }

                %>
                <!--Respuesta Correcta-->
                <div class="media mb-4">
                    <li class="nav-item dropdown ">
                        <a class="nav-link " href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" style="float:right;" >
                            <i class="fas fa-ellipsis-h "></i>
                        </a>
                        <%                            if (UserLogged.compareTo(IdUsuario) == 0) {
                        %>
                        <div class="user-buttons">
                            <button class="btn btn-success btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#ModalEditR<%=IdRespuesta%>" >Editar  <i class="fas fa-user-edit"></i></button>
                            <button class="btn btn-danger btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#ModalDeleteR<%=IdRespuesta%>" >Borrar  <i class="fas fa-trash-alt"></i></button>
                        </div>
                        <%
                            }
                        %>
                        <!--Modal Edit Respuesta Correcta-->
                        <div class="modal fade" id="ModalEditR<%=IdRespuesta%>" tabindex="-1" aria-labelledby="ModalEditLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Editar Respuesta</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <form action="pregunta.jsp?idp=<%=idPregunta%>" class="was-validated" method="get">
                                        <div class="modal-body">


                                            <legend>Llena los campos para editar tu Respuesta</legend>

                                            <div class="mb-3">
                                                <label for="descripcion" class="form-label">Respuesta</label>
                                                <textarea class="form-control" id="respuestaREM" name ="respuestaREM"  rows="3"><%=Respuesta%></textarea>
                                            </div>
                                            <div class="mb-3">
                                                <label for="formFile" class="form-label">Seleccione una imagen para su pregunta</label>
                                                <input class="form-control" type="file" id="fotoREM" value="" name="fotoEM">
                                            </div>
                                            <button type="submit" name="enviarREM<%=IdRespuesta%>" class="btn btn-primary">Guardar cambios</button>
                                            <input  type="hidden" id="idp" name="idp" value="<%=idPregunta%>">
                                            <input  type="hidden" id="idr" name="idr" value="<%=IdRespuesta%>">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <%
                            if (request.getParameter("enviarREM" + IdRespuesta) != null) {
                                try {
                                    String RespuestaRE = request.getParameter("respuestaREM");
                                    String idrE = request.getParameter("idr");
                                    String ImagenRE = request.getParameter("fotoREM");
                                    Class.forName("com.mysql.jdbc.Driver");

                                    con = DriverManager.getConnection(url, "root", "root");
                                    st = con.createStatement();

                                    st.executeUpdate("CALL sp_Respuestas('U'," + idrE + ",0,'" + RespuestaRE + "','" + ImagenRE + "',1,1,1,0)");

                                    // request.getRequestDispatcher("pregunta.jsp?idp=" + idPregunta);
                                } catch (Exception e) {
                                    out.print("error mysql 3.2.1" + e);
                                } finally {
                                    try {
                                        con.close();
                                    } catch (SQLException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                }
                            }
                        %>                
                        <!--Modal Edit Respuesta Correcta-->


                        <!--Modal Delete Respuesta Correcta-->
                        <div class="modal fade" id="ModalDeleteR<%=IdRespuesta%>" tabindex="-1" aria-labelledby="ModalDeleteLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">¿Quieres borrar tu respuesta?</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <form action="pregunta.jsp?idp=<%=idPregunta%>" class="was-validated" method="GET">
                                        <br>

                                        <div style=" text-align: center;">
                                            <button type="submit" name="enviarrDM<%=IdRespuesta%>" class="btn btn-primary"  >Borrar</button>
                                            <input  type="hidden" id="idp" name="idp" value="<%=idPregunta%>">
                                            <input  type="hidden" id="idr" name="idr" value="<%=IdRespuesta%>">
                                        </div>
                                        <br>
                                        <br>
                                    </form>

                                </div>
                            </div>
                        </div> 
                        <%
                            if (request.getParameter("enviarrDM" + IdRespuesta) != null) {
                                try {
                                    String idr = request.getParameter("idr");
                                    Class.forName("com.mysql.jdbc.Driver");

                                    con = DriverManager.getConnection(url, "root", "root");
                                    st = con.createStatement();

                                    st.executeUpdate("CALL sp_Respuestas('B'," + idr + ",0,'','',0,0,0,0)");
                                    request.getRequestDispatcher("pregunta.jsp?idp=" + idPregunta);

                                } catch (Exception e) {
                                    out.print("error 3.3" + e);
                                } finally {
                                    try {
                                        con.close();
                                    } catch (SQLException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                }
                            }
                        %>
                        <!--Modal Delete Respuesta Correcta-->

                    </li>
                    <p class="lead"> por <img src="data:image/gif;base64,<%= imgDataBase642%>" alt="" style="width:50px; border-radius:25px;padding-left:5px;padding-right:5px;"><%if (UserLogged.compareTo(IdUsuario) == 0) {%><a href="perfilpropio.jsp"><%} else {%><a href="perfilajeno.jsp?idu=<%=IdUsuario%>"><%}%><%=UserDBRC%></a> </p>

                    <div class="media-body">
                        <%= Respuesta%>
                    </div>
                    <!--Imagen de la publicacion-->
                    <img class="img-fluid rounded" src="data:image/gif;base64,<%= imgDataBase645%>" alt="" style="width:300px;">
                    <br>
                    <!--Ratings de la publicacion-->

                    <div class="user-buttons">
                        <form action="pregunta.jsp?idp=<%=idPregunta%>" class="was-validated" method="GET">
                            <br>
                            <input  type="hidden" id="idp" name="idp" value="<%=idPregunta%>">
                            <input  type="hidden" id="idr" name="idr" value="<%=IdRespuesta%>">
                            <button class="btn btn-primary btn-sm" type="submit" name="UtilR<%=IdRespuesta%>" ><i class="fas fa-thumbs-up"></i><%=numeroUtilRC%></button>
                            <button class="btn btn-primary btn-sm" type="submit" name="NoUtilR<%=IdRespuesta%>" ><i class="fas fa-thumbs-down"></i><%=numeroNoUtilRC%></button>

                        </form>
                        <br>

                        <%
                            if (request.getParameter("UtilR" + IdRespuesta) != null) {
                                if (UserLoggedI != 0) {

                                    String idr = request.getParameter("idr");
                                    Boolean condition;
                                    try {
                                        con = DriverManager.getConnection(url, "root", "root");
                                        st = con.createStatement();
                                        rs = st.executeQuery("SELECT * FROM respuestas_valoradas Where Id_Respuesta =" + idr + " and Id_Usuario=" + UserLogged);
                                        if (rs.next()) {
                                            Boolean isUtil = rs.getBoolean(4);
                                            if (isUtil == true) {
                                                st = con.createStatement();
                                                st.executeUpdate("DELETE FROM respuestas_valoradas WHERE Id_Respuesta= " + idr + " and Id_Usuario=" + UserLogged);
                                            } else {
                                                st = con.createStatement();
                                                st.executeUpdate("Update respuestas_valoradas SET Util=1,No_Util=0 WHERE Id_Respuesta= " + idr + " and Id_Usuario=" + UserLogged);
                                            }
                                        } else {
                                            st = con.createStatement();
                                            st.executeUpdate("INSERT into respuestas_valoradas(Id_Respuesta,Id_Usuario,Util,No_Util) values(" + idr + "," + UserLogged + ",1,0)");
                                        }

                                    } catch (Exception e) {
                                        out.print("error mysql respuestasc util" + e);
                                    } finally {
                                        try {
                                            con.close();
                                        } catch (SQLException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        %>
                        <%
                            if (request.getParameter("NoUtilR" + IdRespuesta) != null) {
                                if (UserLoggedI != 0) {
                                    String idr = request.getParameter("idr");
                                    Boolean condition;
                                    try {
                                        con = DriverManager.getConnection(url, "root", "root");
                                        st = con.createStatement();
                                        rs = st.executeQuery("SELECT * FROM respuestas_valoradas Where Id_Respuesta =" + idr + " and Id_Usuario=" + UserLogged);
                                        if (rs.next()) {
                                            Boolean isNoUtil = rs.getBoolean(5);
                                            if (isNoUtil == true) {
                                                st = con.createStatement();
                                                st.executeUpdate("DELETE FROM respuestas_valoradas WHERE Id_Respuesta= " + idr + " and Id_Usuario=" + UserLogged);

                                            } else {
                                                st = con.createStatement();
                                                st.executeUpdate("Update respuestas_valoradas SET Util=0,No_Util=1 WHERE Id_Respuesta= " + idr + " and Id_Usuario=" + UserLogged);

                                            }
                                        } else {
                                            st = con.createStatement();
                                            st.executeUpdate("INSERT into respuestas_valoradas(Id_Respuesta,Id_Usuario,Util,No_Util) values(" + idr + "," + UserLogged + ",0,1)");
                                        }

                                    } catch (Exception e) {
                                        out.print("error mysql respuestasc no utiles" + e);
                                    } finally {
                                        try {
                                            con.close();
                                        } catch (SQLException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        %>

                    </div>
                    <ul class="nav  navbar-light bg-light">

                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page"  href="#"><i class="fas fa-check"></i> Respuesta correcta</a>
                        </li>

                    </ul>
                    <!--Respuesta correcta-->


                    <br>
                    <hr>
                </div>
                <!--Respuesta Correcta-->

                <%
                        }

                    } catch (Exception e) {
                        out.print("error respuestas correctas" + e);
                    } finally {
                        try {
                            con.close();
                        } catch (SQLException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                %>


                <%
                    String IdRespuestaR = "";
                    String IdUsuarioR = "";
                    String RespuestaR = "";
                    String ImagenR = "";
                    Boolean EditadaR = false;
                    Boolean ActivoR = true;

                    int numeroUtilR = 0;
                    int numeroNoUtilR = 0;

                    String FotoUR = "";
                    String UserDBR = "";
                    Blob ImagenP3 = null;
                    String imgDataBase643 = "";
                    Blob ImagenP6 = null;
                    String imgDataBase646 = "";
                    try {

                        Class.forName("com.mysql.jdbc.Driver");

                        con = DriverManager.getConnection(url, "root", "root");
                        st = con.createStatement();
                        rs = st.executeQuery("SELECT * FROM respuestas WHERE Activo = true and Correcta = false and Id_Pregunta =" + idPregunta);

                        while (rs.next()) {

                            IdRespuestaR = rs.getString(1);
                            IdUsuarioR = rs.getString(2);
                            RespuestaR = rs.getString(3);
                            ImagenR = rs.getString(4);
                            EditadaR = rs.getBoolean(5);
                            ActivoR = rs.getBoolean(6);
                            ImagenP6 = rs.getBlob(4);

                            byte[] imgData6 = null;

                            imgData6 = ImagenP6.getBytes(1, (int) ImagenP6.length());

                            imgDataBase646 = new String(Base64.getEncoder().encode(imgData6));

                            st = con.createStatement();
                            //rsr2 = st.executeQuery("SELECT count(Util) as Util, count(No_Util) as No_Util FROM respuestas_valoradas WHERE Id_Respuesta = " + IdRespuestaR);
                            rsr2 = st.executeQuery("SELECT Util , No_Util  FROM respuestas_valoradas WHERE Id_Respuesta = " + IdRespuestaR);
                            numeroUtilR = 0;
                            numeroNoUtilR = 0;
                            while (rsr2.next()) {
                                if (rsr2.getBoolean(1) == true) {
                                    numeroUtilR++;
                                }
                                if (rsr2.getBoolean(2) == true) {
                                    numeroNoUtilR++;
                                }
                            }

                            st = con.createStatement();
                            rsr = st.executeQuery("SELECT * FROM usuarios WHERE IdUsuarios =" + IdUsuarioR);
                            if (rsr.next()) {
                                FotoUR = rsr.getString(6);
                                UserDBR = rsr.getString(8);
                                ImagenP3 = rsr.getBlob(6);

                                byte[] imgData3 = null;

                                imgData3 = ImagenP3.getBytes(1, (int) ImagenP3.length());

                                imgDataBase643 = new String(Base64.getEncoder().encode(imgData3));
                            }


                %>
                <div class="media mb-4">


                    <%                            if (UserLogged.compareTo(IdUsuarioR) == 0) {
                    %>
                    <div class="user-buttons">

                        <button class="btn btn-success btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#ModalEditR<%=IdRespuestaR%>" >Editar  <i class="fas fa-user-edit"></i></button>
                        <button class="btn btn-danger btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#ModalDeleteR<%=IdRespuestaR%>" >Borrar  <i class="fas fa-trash-alt"></i></button>
                            <%
                                }
                                if (UserLogged.compareTo(IdUser) == 0) {
                            %>
                        <button class="btn btn-primary btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#ModalCorrectR<%=IdRespuestaR%>" >Respuesta Correcta  <i class="fas fa-check"></i></button>                 
                            <%
                                }
                            %>
                    </div>

                    <!--Modal Edit Respuestas -->
                    <div class="modal fade" id="ModalEditR<%=IdRespuestaR%>" tabindex="-1" aria-labelledby="ModalEditLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Editar Respuesta</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form action="pregunta.jsp?idp=<%=idPregunta%>" class="was-validated" method="POST">
                                    <div class="modal-body">


                                        <legend>Llena los campos para editar tu Respuesta</legend>

                                        <div class="mb-3">
                                            <label for="descripcion" class="form-label">Respuesta</label>
                                            <textarea class="form-control" id="respuestaREM" name ="respuestaREM"  rows="3"><%=RespuestaR%></textarea>
                                        </div>
                                        <div class="mb-3">
                                            <label for="formFile" class="form-label">Seleccione una imagen para su pregunta</label>
                                            <input class="form-control" type="file" id="fotoREM" value="" name="fotoEM">
                                        </div>
                                        <button type="submit" name="enviarREM<%=IdRespuestaR%>" class="btn btn-primary">Guardar cambios</button>
                                        <input  type="hidden" id="idp" name="idp" value="<%=idPregunta%>">
                                        <input  type="hidden" id="idr" name="idr" value="<%=IdRespuestaR%>">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <%
                        if (request.getParameter("enviarREM" + IdRespuestaR) != null) {
                            try {
                                String RespuestaRE = request.getParameter("respuestaREM");
                                String idrE = request.getParameter("idr");
                                String ImagenRE = request.getParameter("fotoREM");
                                Class.forName("com.mysql.jdbc.Driver");

                                con = DriverManager.getConnection(url, "root", "root");
                                st = con.createStatement();

                                st.executeUpdate("CALL sp_Respuestas('U'," + idrE + ",0,'" + RespuestaRE + "','" + ImagenRE + "',1,1,1,0)");

                                // request.getRequestDispatcher("pregunta.jsp?idp=" + idPregunta);
                            } catch (Exception e) {
                                out.print("error mysql 3.2.1" + e);
                            } finally {
                                try {
                                    con.close();
                                } catch (SQLException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                        }
                    %>                
                    <!--Modal Edit Respuestas -->


                    <!--Modal Delete Respuestas -->
                    <div class="modal fade" id="ModalDeleteR<%=IdRespuestaR%>" tabindex="-1" aria-labelledby="ModalDeleteLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">¿Quieres borrar tu respuesta?</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form action="pregunta.jsp?idp=<%=idPregunta%>" class="was-validated" method="POST">
                                    <br>

                                    <div style=" text-align: center;">
                                        <button type="submit" name="enviarrDM<%=IdRespuestaR%>" class="btn btn-primary"  >Borrar</button>
                                        <input  type="hidden" id="idp" name="idp" value="<%=idPregunta%>">
                                        <input  type="hidden" id="idr" name="idr" value="<%=IdRespuestaR%>">
                                    </div>
                                    <br>
                                    <br>
                                </form>

                            </div>
                        </div>
                    </div> 
                    <%
                        if (request.getParameter("enviarrDM" + IdRespuestaR) != null) {
                            try {
                                String idr = request.getParameter("idr");
                                Class.forName("com.mysql.jdbc.Driver");

                                con = DriverManager.getConnection(url, "root", "root");
                                st = con.createStatement();

                                st.executeUpdate("CALL sp_Respuestas('B'," + idr + ",0,'','',0,0,0,0)");
                                request.getRequestDispatcher("pregunta.jsp?idp=" + idPregunta);

                            } catch (Exception e) {
                                out.print("error 3.3" + e);
                            } finally {
                                try {
                                    con.close();
                                } catch (SQLException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                        }
                    %>                
                    <!--Modal Delete Respuestas -->
                    <!--Modal Correct Respuestas -->
                    <div class="modal fade" id="ModalCorrectR<%=IdRespuestaR%>" tabindex="-1" aria-labelledby="ModalDeleteLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Marcar como respuesta correcta</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <form action="pregunta.jsp?idp=<%=idPregunta%>" class="was-validated" method="GET">
                                    <br>

                                    <div style=" text-align: center;">
                                        <button type="submit" name="enviarRCM<%=IdRespuestaR%>" class="btn btn-primary"  >Respuesta Correcta</button>
                                        <input  type="hidden" id="idp" name="idp" value="<%=idPregunta%>">
                                        <input  type="hidden" id="idr" name="idr" value="<%=IdRespuestaR%>">
                                    </div>
                                    <br>
                                    <br>
                                </form>

                            </div>
                        </div>
                    </div> 

                    <%
                        if (request.getParameter("enviarRCM" + IdRespuestaR) != null) {
                            try {
                                String idr = request.getParameter("idr");
                                Class.forName("com.mysql.jdbc.Driver");

                                con = DriverManager.getConnection(url, "root", "root");
                                st = con.createStatement();

                                st.executeUpdate("Update respuestas Set Correcta = 0 Where Id_Pregunta = " + idPregunta);
                                st = con.createStatement();

                                st.executeUpdate("Update respuestas Set Correcta = 1 Where IdRespuestas = " + idr);
                                //request.getRequestDispatcher("pregunta.jsp").forward(request, response);

                            } catch (Exception e) {
                                out.print("error 3.3 dinamico rc" + e);
                            } finally {
                                try {
                                    con.close();
                                } catch (SQLException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                        }
                    %>                  
                    <!--Modal Correct Respuestas -->

                    <p class="lead"> por <img src="data:image/gif;base64,<%= imgDataBase643%>" alt="" style="width:50px; border-radius:25px;padding-left:5px;padding-right:5px;"><%if (UserLogged.compareTo(IdUsuarioR) == 0) {%><a href="perfilpropio.jsp"><%} else {%><a href="perfilajeno.jsp?idu=<%=IdUsuarioR%>"><%}%><%=UserDBR%></a> </p>

                    <div class="media-body">
                        <%=RespuestaR%>
                    </div>
                    <!--Imagen de la publicacion-->
                    <img class="img-fluid rounded" src="data:image/gif;base64,<%= imgDataBase646%>" alt="" style="width:300px;">
                    <br>
                    <!--Ratings de la publicacion-->
                    <div class="user-buttons">
                        <form action="pregunta.jsp?idp=<%=idPregunta%>" class="was-validated" method="GET">
                            <br>
                            <input  type="hidden" id="idp" name="idp" value="<%=idPregunta%>">
                            <input  type="hidden" id="idr" name="idr" value="<%=IdRespuestaR%>">
                            <button class="btn btn-primary btn-sm" type="submit" name="UtilR<%=IdRespuestaR%>" ><i class="fas fa-thumbs-up"></i><%=numeroUtilR%></button>
                            <button class="btn btn-primary btn-sm" type="submit" name="NoUtilR<%=IdRespuestaR%>" ><i class="fas fa-thumbs-down"></i><%=numeroNoUtilR%></button>

                        </form>
                        <br>

                        <%
                            if (request.getParameter("UtilR" + IdRespuestaR) != null) {

                                if (response.isCommitted()) {
                                    out.print("la respuesta ya a sido enviada");
                                }
                                if (UserLoggedI != 0) {
                                    String idr = request.getParameter("idr");
                                    Boolean condition;
                                    try {
                                        con = DriverManager.getConnection(url, "root", "root");
                                        st = con.createStatement();
                                        rsr3 = st.executeQuery("SELECT * FROM respuestas_valoradas Where Id_Respuesta =" + idr + " and Id_Usuario=" + UserLogged);
                                        if (rsr3.next()) {
                                            Boolean isUtil = rsr3.getBoolean(4);
                                            if (isUtil == true) {
                                                st = con.createStatement();
                                                st.executeUpdate("DELETE FROM respuestas_valoradas WHERE Id_Respuesta= " + idr + " and Id_Usuario=" + UserLogged);
                                            } else {
                                                st = con.createStatement();
                                                st.executeUpdate("Update respuestas_valoradas SET Util=1,No_Util=0 WHERE Id_Respuesta= " + idr + " and Id_Usuario=" + UserLogged);
                                            }
                                        } else {
                                            st = con.createStatement();
                                            st.executeUpdate("INSERT into respuestas_valoradas(Id_Respuesta,Id_Usuario,Util,No_Util) values(" + idr + "," + UserLogged + ",1,0)");
                                        }

                                        //request.setAttribute("idp", idPregunta);
                                        //request.getRequestDispatcher("pregunta.jsp").forward(request,response);
                                        response.sendRedirect("pregunta.jsp?idp=" + idPregunta);
                                        return;

                                    } catch (Exception e) {
                                        out.print("error mysql respuestas util" + e);
                                    } finally {
                                        try {
                                            con.close();
                                        } catch (SQLException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        %>
                        <%
                            if (request.getParameter("NoUtilR" + IdRespuestaR) != null) {
                                if (UserLoggedI != 0) {
                                    String idr = request.getParameter("idr");
                                    Boolean condition;
                                    try {
                                        con = DriverManager.getConnection(url, "root", "root");
                                        st = con.createStatement();
                                        rsr3 = st.executeQuery("SELECT * FROM respuestas_valoradas Where Id_Respuesta =" + idr + " and Id_Usuario=" + UserLogged);
                                        if (rsr3.next()) {
                                            Boolean isNoUtil = rsr3.getBoolean(5);
                                            if (isNoUtil == true) {
                                                st = con.createStatement();
                                                st.executeUpdate("DELETE FROM respuestas_valoradas WHERE Id_Respuesta= " + idr + " and Id_Usuario=" + UserLogged);

                                            } else {
                                                st = con.createStatement();
                                                st.executeUpdate("Update respuestas_valoradas SET Util=0,No_Util=1 WHERE Id_Respuesta= " + idr + " and Id_Usuario=" + UserLogged);

                                            }
                                        } else {
                                            st = con.createStatement();
                                            st.executeUpdate("INSERT into respuestas_valoradas(Id_Respuesta,Id_Usuario,Util,No_Util) values(" + idr + "," + UserLogged + ",0,1)");

                                        }

                                    } catch (Exception e) {
                                        out.print("error mysql respuestas no utiles" + e);
                                    } finally {
                                        try {
                                            con.close();
                                        } catch (SQLException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        %>

                    </div>

                    <!--Respuesta correcta-->
                    <br>
                    <hr>
                </div>
                <%                }
                    } catch (Exception e) {
                        out.print("error mysql 7 " + IdRespuestaR + e);
                    } finally {
                        try {
                            con.close();
                        } catch (SQLException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                %>        
                <!--Respuestas-->
            </div>
        </div>
        <!--Pregunta-->





        <!--Footer-->
        <footer class="page-footer bg-dark">
            <div style="background-color:#ea4f02">
                <div class="container">
                    <div class="row py-4 d-flex align-items-center">
                        <div class="col-md-12 text-center">
                            <a href="#"><i class="fab fa-facebook-square text-white mr-3"></i></a>
                            <a href="#"><i class="fab fa-twitter-square text-white mr-3"></i></a>
                            <a href="#"><i class="fab fa-instagram-square text-white"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container text-center  text-light text-md-left mt-5">
                <div class="row">
                    <div class="col-md-3 mx-auto mb-4">
                        <h6 class="text-uppercase font-weight-bold">Desarrolladores:</h6>
                        <hr class="bg-success mb-4 mt-0 d-inline-block mx-auto" style="width: 125px; height: 2px;">
                        <p class="mt-1">Fernando Moncayo Marquez</p>
                        <a class="mt-1 text-white" href="#" >fermoncam506@gmail.com</a>
                    </div>

                    <div class="col-md-2 mx-auto mb-4">
                        <h6 class="text-uppercase font-weight-bold">Categorias:</h6>
                        <hr class="bg-success mb-4 mt-0 d-inline-block mx-auto" style="width: 85px; height: 2px;">

                        <ul class="list-unstyled ">
                            <%
                                try {
                                    Class.forName("com.mysql.jdbc.Driver");

                                    con = DriverManager.getConnection(url, "root", "root");
                                    st = con.createStatement();
                                    rs = st.executeQuery("SELECT * FROM categorias");

                                    while (rs.next()) {

                            %>
                            <li class="my-2" ><a href="busqueda.jsp?id=<%=rs.getInt(1)%>" class="text-white"><%=  rs.getString(2)%></a></li>
                                <%
                                        }
                                    } catch (Exception e) {
                                        out.print("error mysql 8" + e);
                                    } finally {
                                        try {
                                            con.close();
                                        } catch (SQLException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                    }
                                %>
                        </ul>

                    </div>

                    <div class="col-md-2 mx-auto mb-4">
                        <h6 class="text-uppercase font-weight-bold">Repositorio:</h6>
                        <hr class="bg-success mb-4 mt-0 d-inline-block mx-auto" style="width: 110px; height: 2px">
                        <p></p>

                        <a class="mt-1 text-white" href="https://gitlab.com/Fercho_SMT/proyecto_programacion_web"  target = "_blank">Repositorio del proyecto</a>
                    </div>

                    <div class="col-md-2 mx-auto mb-4">
                        <h6 class="text-uppercase font-weight-bold">Ask & Cook:</h6>
                        <hr class="bg-success mb-4 mt-0 d-inline-block mx-auto" style="width: 85px; height: 2px;">
                        <ul class="list-unstyled ">
                            <li class="my-2" ><i class="fas fa-home mr-3"> Monterrey, Nuevo Leon</i></li>
                            <li class="my-2" ><i class="fas fa-envelope mr-3"> askandcook@gmail.com</i></li>
                            <li class="my-2" ><i class="fas fa-phone  mr-3"> 8112298194</i></li>
                        </ul>
                    </div>


                </div>
            </div>

            <div class="footer-copyright text-center py-3 text-white bg-black">
                <p>&copy; Copyright
                    <a href="principal.jsp" class="text-white">askandcook.com</a>
                <p >Diseñado por Ask&Cook</p>
                </p>
            </div>
        </footer>
        <!--Footer-->



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
        <!-- MDB -->
        <script
            type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"
        ></script>

    </body>
</html>
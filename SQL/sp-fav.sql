CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Favoritas`(
OPC 					char(1),
IdPreguntas_Favoritas 	INT,
Pregunta 				INT,
Usuario 				INT,
Favorito 				TINYINT(1)
)
BEGIN
declare IdUSer int;
declare IdQuestion int;

set IdUser = (SELECT Usuarios.IdUsuarios FROM Usuarios WHERE Usuario.Username = Usuario), 
    IdQuestion = (SELECT Preguntas.IdPreguntas FROM Preguntas WHERE Preguntas.Pregunta = Pregunta);

IF OPC = 'I' then	#insert
	begin
		insert into Preguntas_Favoritas (Preguntas_Favoritas.Id_Pregunta, Preguntas_Favoritas.Id_Usuario, 
        Preguntas_Favoritas.Favorito) 
		values (IdQuestion, IdUser, 1);    
	end;
elseif OPC = 'B' then	#baja_logica
	begin
		update Preguntas_Favoritas 
		set Preguntas_Favoritas.Favorito = 0
			where Preguntas_Favoritas.IdPreguntas_Favoritas = IdPreguntas_Favoritas;
    end;
 end if;   
END
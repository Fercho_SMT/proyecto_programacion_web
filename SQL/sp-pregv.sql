CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_PreguntasValoradas`(
OPC 					char(1),
IdPreguntas_valoradas	INT,
Pregunta 				INT,
Usuario 				INT,
Util 					TINYINT(1),
No_Util 				TINYINT(1)
)
BEGIN
declare IdUSer int;
declare IdQuestion int;

set IdUser = (SELECT Usuarios.IdUsuarios FROM Usuarios WHERE Usuario.Username = Usuario), 
    IdQuestion = (SELECT Preguntas.IdPreguntas FROM Preguntas WHERE Preguntas.Pregunta = Pregunta);

IF OPC = 'I' then	#insert
	begin
		insert into Preguntas_Valoradas (Preguntas_Valoradas.Id_Pregunta, Preguntas_Valoradas.Id_Usuario, 
        Preguntas_Valoradas.Util, Preguntas_Valoradas.No_Util) 
		values (IdQuestion, IdUser, Util, No_Util);    
	end;
elseif OPC = 'D' then	#delete
	begin
		delete
        from Preguntas_Valoradas 
		where Preguntas_Valoradas.IdPreguntas_valoradas = IdPreguntas_valoradas;
    end;
end if;
END
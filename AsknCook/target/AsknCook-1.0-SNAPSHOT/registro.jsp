<%-- 
    Document   : registro
    Created on : 25/05/2021, 09:01:29 PM
    Author     : Usuario
--%>

<%@page import="java.io.InputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.util.Base64"%>
<%@page import="java.sql.*"%>
<%@page import="clases.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Registro</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

        <link rel="shortcut icon" type="image/x-icon" href="./images/askandcook.png">
        <link rel="stylesheet" type="text/css" href="css/registro.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <!--Footer-->
        <!-- Font Awesome -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css"
            rel="stylesheet"
            />
        <!--Footer-->

        <style>
            .bg{
                background-image: url(./images/coocking_register_2.jpg);
                background-position: center ;
                background-size: 100%;
                border-radius: 10px 0px 0px 10px;
            }
        </style>

    </head>
    <body>
        <%
            Connection con = null;
            Statement st = null;
            ResultSet rs = null;

            String servername = "localhost";
            String port = "3306";
            String databaseName = "db_ask_n_cook";
            String url = "jdbc:mysql://" + servername + ":" + port + "/" + databaseName;
            int UserLoggedI;
            String UserLogged = "0";
            User u = (User) session.getAttribute("User");
            if (u != null) {
                UserLoggedI = u.getId();
                UserLogged = Integer.toString(UserLoggedI);

            } else {
                UserLoggedI = 0;
            }

        %>
        <!--Header-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="position: fixed; width: 100%; top:0px; z-index: 1000;">
            <div class="container-fluid">
                <a class="navbar-brand" href="principal.jsp"><img src="./images/askandcook.png" width="50" alt="Error de carga"></a>
                <button class="navbar-toggler " type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon bg-dark"></span>
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="principal.jsp">Inicio</a>

                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" <%if (UserLoggedI == 0) {%> href="inicio.jsp"  <%} else {%> href="perfilpropio.jsp?idu=<%=UserLoggedI%>"<%}%>>Perfil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="busquedap.jsp">Busqueda Avanzada</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Categorias
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <%
                                    try {
                                        Class.forName("com.mysql.jdbc.Driver");

                                        con = DriverManager.getConnection(url, "root", "root");
                                        st = con.createStatement();
                                        rs = st.executeQuery("SELECT * FROM categorias");

                                        while (rs.next()) {
                                %>
                                <li><a class="dropdown-item" href="busquedap.jsp"><%=rs.getString(2)%></a></li>
                                    <%                                                      }
                                        } catch (Exception e) {
                                            out.print("error mysql" + e);
                                        } finally {
                                            try {
                                                con.close();
                                            } catch (SQLException e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                            }
                                        }
                                    %>                 
                            </ul>
                        </li>
                    </ul>
                    <form action="busqueda.jsp" class="was-validated d-flex" method="POST">
                        <input class="form-control me-2" id="buscarpreg" name="buscarpreg" type="search" placeholder="Escribe para buscar" aria-label="Search">
                        <button class="btn btn-outline-success" name="enviarbus" type="submit">Buscar</button>
                    </form>
                </div>
            </div>
        </nav>


        <!--Header-->

        <div>

        </div>

        <!--Registro-->
        <br>
        <div class="container bg-dark w-75 mt-5 rounded shadow  ">
            <div class="row align-items-stretch">
                <div class="col">
                    <div class="text-end">
                        <p></p>
                        <img src="./images/askandcook.png" width="48px" alt="Error de carga">
                        <h2 class="fw-bold text-center text-white">Bienvenido</h2>
                        <h4 class="fw-bold text-center py-5 text-white">Registra tu cuenta aquí</h4>
                        <form  action="registro.jsp" class="was-validated" method="POST" >
                            <div class="mb-4 text-start text-white">
                                <div class="form-group">
                                    <label for="text" class="form-label text-white ">Nombre:</label>
                                    <input type="text" class="form-control " id="name" name="name" placeholder="Coloca tu nombre" required>
                                    <div class="valid-feedback"> El campo se lleno correctamente</div>
                                    <div class="invalid-feedback">El campo no se lleno correctamente</div>
                                </div>
                            </div>

                            <div class="mb-4 text-start text-white">
                                <div class="form-group">
                                    <label for="text" class="form-label text-white ">Apellido Paterno:</label>
                                    <input type="text" class="form-control " id="fname" name="fname" placeholder="Coloca tu apellido paterno" required>
                                    <div class="valid-feedback"> El campo se lleno correctamente</div>
                                    <div class="invalid-feedback">El campo no se lleno correctamente</div>
                                </div>
                            </div>

                            <div class="mb-4 text-start text-white">
                                <div class="form-group">
                                    <label for="text" class="form-label text-white ">Apellido Materno:</label>
                                    <input type="text" class="form-control " id="lname" name="lname" placeholder="Coloca tu apellido materno" required>
                                    <div class="valid-feedback"> El campo se lleno correctamente</div>
                                    <div class="invalid-feedback">El campo no se lleno correctamente</div>
                                </div>
                            </div>

                            <div class="mb-4 text-start text-white">
                                <div class="form-group">
                                    <label for="date" class="form-label text-white ">Fecha de nacimiento:</label>
                                    <input type="date" class="form-control " id="birthday" name="birthday" required>
                                    <div class="valid-feedback"> El campo se lleno correctamente</div>
                                    <div class="invalid-feedback">El campo no se lleno correctamente</div>
                                </div>
                            </div>

                            <div class="mb-4 text-start text-white">
                                <div class="form-group">
                                    <label for="email" class="form-label text-white ">Correo electronico:</label>
                                    <input type="email" class="form-control " id="email" name="email" placeholder="Coloca tu correo electronico" required>
                                    <div class="valid-feedback"> El campo se lleno correctamente</div>
                                    <div class="invalid-feedback">El campo no se lleno correctamente</div>
                                </div>
                            </div>

                            <div class="mb-4 text-start text-white">
                                <div class="form-group">
                                    <label for="text" class="form-label text-white ">Nombre de usuario:</label>
                                    <input type="text" class="form-control " id="username" name="username" placeholder="Escoge un nombre de usuario" required>
                                    <div class="valid-feedback"> El campo se lleno correctamente</div>
                                    <div class="invalid-feedback">El campo no se lleno correctamente</div>
                                </div>
                            </div>

                            <div class="mb-4 text-start text-white ">
                                <div class="form-group">
                                    <label for="password" class="form-label text-white">Contraseña:</label>
                                    <input type="password" class="form-control" id="password" name="password"  placeholder="Contraseña" required>
                                    <div class="valid-feedback"> El campo se lleno correctamente</div>
                                    <div class="invalid-feedback">El campo no se lleno correctamente</div>
                                </div>

                            </div>

                            <div class="mb-4 text-start text-white ">
                                <div class="form-group">
                                    <label for="password" class="form-label text-white">Confirmar contraseña:</label>
                                    <input type="password" class="form-control" id="cpassword" name="cpassword"  placeholder="Contraseña" required>
                                    <div class="valid-feedback"> El campo se lleno correctamente</div>
                                    <div class="invalid-feedback">Las contraseñas no coinciden</div>
                                </div>

                            </div>
                            <br>

                            <div class="mb-4 text-start text-white"style="width: 50%; position: center;">
                                <label for="image" class="form-label text-white">Foto de perfil:</label>
                                <div class="container">

                                    <div class="wrapper">

                                        <div id="imagen" class="image">
                                            <img id="pfpi" name="pfpi"  src="" alt="">
                                        </div>
                                        <div class="content">
                                            <div class="icon">
                                                <i class="fas fa-cloud-upload-alt"></i></div>
                                            <div class="text">
                                                Ningun archivo seleccionado todavia</div>
                                        </div>
                                        <div id="cancel-btn">
                                            <i class="fas fa-times"></i></div>
                                        <div class="file-name">
                                            Nombre Archivo</div>
                                    </div>
                                    
                                    <button form="" formaction="" onclick="defaultBtnActive()" id="custom-btn" style="max-height: 40px;">Escoge un archivo</button>
                                    <input name="pfp" id="default-btn" type="file" hidden>
                                </div>
                            </div>
                            <script>
                                const wrapper = document.querySelector(".wrapper");
                                const fileName = document.querySelector(".file-name");
                                const defaultBtn = document.querySelector("#default-btn");
                                const customBtn = document.querySelector("#custom-btn");
                                const cancelBtn = document.querySelector("#cancel-btn i");
                                const img = document.querySelector("#imagen img");
                                let regExp = /[0-9a-zA-Z\^\&\'\@\{\}\[\]\,\$\=\!\-\#\(\)\.\%\+\~\_ ]+$/;
                                function defaultBtnActive() {
                                    defaultBtn.click();
                                }
                                defaultBtn.addEventListener("change", function () {
                                    const file = this.files[0];
                                    if (file) {
                                        const reader = new FileReader();
                                        reader.onload = function () {
                                            const result = reader.result;
                                            img.src = result;
                                            wrapper.classList.add("active");
                                        }
                                        cancelBtn.addEventListener("click", function () {
                                            img.src = "";
                                            wrapper.classList.remove("active");
                                        })
                                        reader.readAsDataURL(file);
                                    }
                                    if (this.value) {
                                        let valueStore = this.value.match(regExp);
                                        fileName.textContent = valueStore;
                                    }
                                });
                            </script>

                            <div class="mb-4"></div>
                            <br>
                            <div class="d-grid">
                                <input type="hidden" name="enviar" id="enviar">
                                <button type="submit" class="btn btn-success"> Registrarte </button>
                            </div>


                            <br>

                        </form>

                    </div>
                </div>
            </div>


        </div>

        <%
            if (request.getParameter("enviar") != null) {

                Class.forName("com.mysql.jdbc.Driver");

                con = DriverManager.getConnection(url, "root", "root");

                String name = request.getParameter("name");
                String fname = request.getParameter("fname");
                String lname = request.getParameter("lname");
                String birthday = request.getParameter("birthday");
                String email = request.getParameter("email");
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                String cpassword = request.getParameter("cpassword");
                String pfp = request.getParameter("pfpi");
                /*Part FilePart = null;
                InputStream imagenPFP = null;
                try {
                    //FilePart = request.getPart("pfp");
                    request.getParameter("pfp");
                    imagenPFP = FilePart.getInputStream();

                } catch (Exception e) {
                    out.print("error filepart" + e);
                }*/
                

                Boolean condition;
                Boolean conditionPass;
                Boolean conditionUser;
                Boolean conditionEmail;
                condition = true;
                conditionPass = true;
                conditionUser = true;
                conditionEmail = true;
                if (password.compareTo(cpassword) == 1) {
                    condition = false;
                    conditionPass = false;
                }

                String userDB;
                String emailDB;
                try {
                    
                    st = con.createStatement();
                    rs = st.executeQuery("SELECT Correo FROM usuarios WHERE Correo = '" + email + "';");

                    if (rs.next()) {
                        emailDB = rs.getString(1);
                        if (email.compareTo(emailDB) == 0) {
                            condition = false;
                            conditionEmail = false;
                        }
                    }

                } catch (Exception e) {
                    out.print("error mysql" + e);
                } finally {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                if (condition == true) {
                    try {

                        con = DriverManager.getConnection(url, "root", "root");
                        st = con.createStatement();
                        /*if (FilePart.getName() != "") {
                            imagenPFP = FilePart.getInputStream();
                        } else {
                            File predeter = new File("C:/Users/FerMo/Desktop/Universidad/sexto semestre/Programacion web/Proyecto Programacion Web/proyecto_programacion_web/AsknCook/src/main/webapp/images/defaultpfp.jpg");

                            imagenPFP = new FileInputStream(predeter);
                        }*/

                        st.executeUpdate("CALL sp_Usuarios('I',0,'" + name + "','" + fname + "','" + lname + "','" + birthday + "','" + pfp
                                + "','" + email + "','" + username + "','" + password + "','2021-03-06',1 );");
                        request.getRequestDispatcher("/inicio.jsp").forward(request, response);
                    } catch (Exception e) {
                        out.print("error mysql" + e);
                    } finally {
                        try {
                            con.close();
                        } catch (SQLException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                } else {
                    if (conditionUser == false) {

        %>

        <%    }
            if (conditionPass == false) {

        %>

        <%    }
            if (conditionEmail == false) {
        %>

        <%
                    }
                }
            }
        %>
        +
        <!--Registro-->

        <br>
        <br>

        <!--Footer-->
        <footer class="page-footer bg-dark">
            <div style="background-color:#ea4f02">
                <div class="container">
                    <div class="row py-4 d-flex align-items-center">
                        <div class="col-md-12 text-center">
                            <a href="#"><i class="fab fa-facebook-square text-white mr-3"></i></a>
                            <a href="#"><i class="fab fa-twitter-square text-white mr-3"></i></a>
                            <a href="#"><i class="fab fa-instagram-square text-white"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container text-center  text-light text-md-left mt-5">
                <div class="row">
                    <div class="col-md-3 mx-auto mb-4">
                        <h6 class="text-uppercase font-weight-bold">Desarrolladores:</h6>
                        <hr class="bg-success mb-4 mt-0 d-inline-block mx-auto" style="width: 125px; height: 2px;">
                        <p class="mt-1">Fernando Moncayo Marquez</p>
                        <a class="mt-1 text-white" href="#" >fermoncam506@gmail.com</a>

                    </div>

                    <div class="col-md-2 mx-auto mb-4">
                        <h6 class="text-uppercase font-weight-bold">Categorias:</h6>
                        <hr class="bg-success mb-4 mt-0 d-inline-block mx-auto" style="width: 85px; height: 2px;">

                        <ul class="list-unstyled ">
                            <%
                                try {
                                    Class.forName("com.mysql.jdbc.Driver");

                                    con = DriverManager.getConnection(url, "root", "root");
                                    st = con.createStatement();
                                    rs = st.executeQuery("SELECT * FROM categorias");

                                    while (rs.next()) {

                            %>
                            <li class="my-2" ><a href="busqueda.jsp?id=<%=rs.getInt(1)%>" class="text-white"><%=  rs.getString(2)%></a></li>
                                <%
                                        }
                                    } catch (Exception e) {
                                        out.print("error mysql" + e);
                                    } finally {
                                        try {
                                            con.close();
                                        } catch (SQLException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                    }
                                %>
                        </ul>

                    </div>

                    <div class="col-md-2 mx-auto mb-4">
                        <h6 class="text-uppercase font-weight-bold">Repositorio:</h6>
                        <hr class="bg-success mb-4 mt-0 d-inline-block mx-auto" style="width: 110px; height: 2px">
                        <p></p>

                        <a class="mt-1 text-white" href="https://gitlab.com/Fercho_SMT/proyecto_programacion_web"  target = "_blank">Repositorio del proyecto</a>
                    </div>

                    <div class="col-md-2 mx-auto mb-4">
                        <h6 class="text-uppercase font-weight-bold">Ask & Cook:</h6>
                        <hr class="bg-success mb-4 mt-0 d-inline-block mx-auto" style="width: 85px; height: 2px;">
                        <ul class="list-unstyled ">
                            <li class="my-2" ><i class="fas fa-home mr-3"> Monterrey, Nuevo Leon</i></li>
                            <li class="my-2" ><i class="fas fa-envelope mr-3"> askandcook@gmail.com</i></li>
                            <li class="my-2" ><i class="fas fa-phone  mr-3"> 8112298194</i></li>
                        </ul>
                    </div>


                </div>
            </div>

            <div class="footer-copyright text-center py-3 text-white bg-black">
                <p>&copy; Copyright
                    <a href="principal.jsp" class="text-white">askandcook.com</a>
                <p >Diseñado por Ask&Cook</p>
                </p>
            </div>
        </footer>
        <!--Footer-->



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
        <!-- MDB -->
        <script
            type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"
        ></script>

    </body>
</html>
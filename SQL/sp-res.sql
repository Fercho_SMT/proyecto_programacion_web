CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Respuestas`(
OPC 				char(1),
IdRespuestas 		INT,
Usuario 			INT,
Respuesta 			VARCHAR(200),
Imagen 				mediumblob,
Editada 			TINYINT(1),
Activo 				TINYINT(1),
Pregunta 			INT,
Correcta			TINYINT(1)
)
BEGIN
declare IdUSer int;
declare IdQuestion int;

set IdUser = (SELECT Usuarios.IdUsuarios FROM Usuarios WHERE Usuarios.IdUsuarios = Usuario), 
    IdQuestion = (SELECT Preguntas.IdPreguntas FROM Preguntas WHERE Preguntas.IdPreguntas = Pregunta);
    
IF OPC = 'I' then	#insert
	begin
		insert into Respuestas (Respuestas.Id_Usuario, Respuestas.Respuesta, Respuestas.Imagen, Respuestas.Editada, 
        Respuestas.Activo, Respuestas.Id_Pregunta, Respuestas.Correcta) 
		values (IdUser, Respuesta, Imagen, 0, 1, IdQuestion, 0);    
	end;
elseif OPC = 'U' then	#update
	begin
		update Respuestas 
		set Respuestas.Respuesta = Respuesta, 
			Respuestas.Imagen =Imagen, 
			Respuestas.Editada =1, 
			Respuestas.Correcta = Correcta
            where Respuestas.IdRespuestas = IdRespuestas;
	end;
elseif OPC = 'B' then	#baja_logica
	begin
		update Respuestas 
		set Respuestas.Activo = 0
			where Respuestas.IdRespuestas = IdRespuestas;
    end;
end if;

END
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Usuarios`(
OPC 				char(1),
IdUsuarios			INT,
Nombre 				VARCHAR(30),
Apellido_P 			VARCHAR(30),
Apellido_M 			VARCHAR(30),
Fecha_Nacimiento 	DATE,
Imagen_Perfil 		mediumblob,
Correo 				VARCHAR(50),
Username 			VARCHAR(20),
Contraseña 			VARCHAR(45),
Fecha_Creacion 		DATE,
Activo 				TINYINT(1)
)
BEGIN
IF OPC = 'I' then	#insert
	begin
		insert into Usuarios (Usuarios.Nombre, Usuarios.Apellido_P, Usuarios.Apellido_M, Usuarios.Fecha_Nacimiento, 
		Usuarios.Imagen_Perfil, Usuarios.Correo, Usuarios.Username, Usuarios.Contraseña, Usuarios.Fecha_Creacion, Usuarios.Activo) 
		values (Nombre, Apellido_P, Apellido_M, Fecha_Nacimiento, Imagen_Perfil, Correo, Username, Contraseña, now() , 1);    
	end;
elseif OPC = 'U' then	#update
	begin
		update Usuarios 
		set Usuarios.Nombre =Nombre,
			Usuarios.Apellido_P =Apellido_P,
			Usuarios.Apellido_M = Apellido_M,
			Usuarios.Fecha_Nacimiento = Fecha_Nacimiento,
			Usuarios.Imagen_Perfil = coalesce(Imagen_Perfil,Usuarios.Imagen_Perfil),
			Usuarios.Username = Username,
			Usuarios.Contraseña = Contraseña
            where Usuarios.IdUsuarios = IdUsuarios;
	end;
elseif OPC = 'B' then	#baja_logica
	begin
		update Usuarios 
		set Usuarios.Activo = 0
			where Usuarios.IdUsuarios = IdUsuarios;
        update preguntas set Preguntas.Activo = 0    Where Preguntas.Id_Usuario = IdUsuarios;
        update respuestas set Respuestas.Activo = 0 Where Respuestas.Id_Usuario = IdUsuarios;
    end;
end if;
END
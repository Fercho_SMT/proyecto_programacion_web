USE `db_ask_n_cook`;
DROP procedure IF EXISTS `sp_Usuarios`;

USE `db_ask_n_cook`;
DROP procedure IF EXISTS `db_ask_n_cook`.`sp_Usuarios`;
;

DELIMITER $$
USE `db_ask_n_cook`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Usuarios`(
OPC 				char(1),
IdUsuarios			INT,
Nombre 				VARCHAR(30),
Apellido_P 			VARCHAR(30),
Apellido_M 			VARCHAR(30),
Fecha_Nacimiento 	DATE,
Imagen_Perfil 		mediumblob,
Correo 				VARCHAR(50),
Username 			VARCHAR(20),
Contraseña 			VARCHAR(45),
Fecha_Creacion 		DATE,
Activo 				TINYINT(1)
)
BEGIN
IF OPC = 'I' then	#insert
	begin
		insert into Usuarios (Usuarios.Nombre, Usuarios.Apellido_P, Usuarios.Apellido_M, Usuarios.Fecha_Nacimiento, 
		Usuarios.Imagen_Perfil, Usuarios.Correo, Usuarios.Username, Usuarios.Contraseña, Usuarios.Fecha_Creacion, Usuarios.Activo) 
		values (Nombre, Apellido_P, Apellido_M, Fecha_Nacimiento, Imagen_Perfil, Correo, Username, Contraseña, now() , 1);    
	end;
elseif OPC = 'U' then	#update
	begin
		update Usuarios 
		set Usuarios.Nombre =Nombre,
			Usuarios.Apellido_P =Apellido_P,
			Usuarios.Apellido_M = Apellido_M,
			Usuarios.Fecha_Nacimiento = Fecha_Nacimiento,
			Usuarios.Imagen_Perfil = Imagen_Perfil,
			Usuarios.Correo = Correo,
			Usuarios.Username = Username,
			Usuarios.Contraseña = Contraseña, 
			Usuarios.Fecha_Creacion =  Fecha_Creacion,
			Usuarios.Activo = Activo
            where Usuarios.IdUsuarios = IdUsuarios;
	end;
elseif OPC = 'B' then	#baja_logica
	begin
		update Usuarios 
		set Usuarios.Activo = 0
			where Usuarios.IdUsuarios = IdUsuarios;
    end;
end if;
END$$

DELIMITER ;
;

# ----------------------------------------------------------------------------------

USE `db_ask_n_cook`;
DROP procedure IF EXISTS `sp_Preguntas`;

DELIMITER $$
USE `db_ask_n_cook`$$
CREATE PROCEDURE `sp_Preguntas` (
OPC 				char(1),
IdPreguntas			INT,
Pregunta 			VARCHAR(100),
Id_Categoria 		INT,
Usuario 			INT,
Descripcion 		VARCHAR(200),
Imagen 				mediumblob,
Editada 			TINYINT(1),
Fecha_Creacion 		DATE,
Activo 				TINYINT(1)
)
BEGIN
declare IdCate int;
declare IdUser int;

set IdCate = (SELECT Categorias.IdCategorias FROM Categorias WHERE Categorias.Nombre_Categoria = Id_Categoria),
	IdUser = (SELECT Usuarios.IdUsuarios FROM Usuarios WHERE Usuario.Username = Usuario);

IF OPC = 'I' then	#insert
	begin
		insert into Preguntas (Preguntas.Pregunta, Preguntas.Id_Categoria, Preguntas.Id_Usuario, Preguntas.Descripcion, 
        Preguntas.Imagen, Preguntas.Editada, Preguntas.Fecha_Creacion, Preguntas.Activo) 
		values (Pregunta, IdCate, IdUser, Descripcion, Imagen, 0, now(), 1);    
	end;
elseif OPC = 'U' then	#update
	begin
		update Preguntas 
		set Preguntas.Pregunta = Pregunta, 
			Preguntas.Id_Categoria = IdCate, 
			Preguntas.Descripcion = Descripcion, 
			Preguntas.Imagen = Imagen, 
			Preguntas.Editada = 1
            where Preguntas.IdPreguntas = IdPreguntas;
	end;
elseif OPC = 'B' then	#baja_logica
	begin
		update Preguntas 
		set Preguntas.Activo = 0
			where Preguntas.IdPreguntas = IdPreguntas;
    end;
end if;
END$$

DELIMITER ;

# ----------------------------------------------------------------------------------

USE `db_ask_n_cook`;
DROP procedure IF EXISTS `sp_Respuestas`;

DELIMITER $$
USE `db_ask_n_cook`$$
CREATE PROCEDURE `sp_Respuestas` (
OPC 				char(1),
IdRespuestas 		INT,
Usuario 			INT,
Respuesta 			VARCHAR(200),
Imagen 				mediumblob,
Editada 			TINYINT(1),
Activo 				TINYINT(1),
Pregunta 			INT,
Correcta			TINYINT(1)
)
BEGIN
declare IdUSer int;
declare IdQuestion int;

set IdUser = (SELECT Usuarios.IdUsuarios FROM Usuarios WHERE Usuarios.Username = Usuario), 
    IdQuestion = (SELECT Preguntas.IdPreguntas FROM Preguntas WHERE Preguntas.Pregunta = Pregunta);
    
IF OPC = 'I' then	#insert
	begin
		insert into Respuestas (Respuestas.Id_Usuario, Respuetas.Respuesta, Respuetas.Imagen, Respuetas.Editada, 
        Respuetas.Activo, Respuetas.Id_Pregunta, Respuetas.Correcta) 
		values (IdUser, Respuesta, Imagen, 0, 1, IdQuestion, null);    
	end;
elseif OPC = 'U' then	#update
	begin
		update Respuestas 
		set Respuetas.Respuesta = Respuesta, 
			Respuetas.Imagen =Imagen, 
			Respuetas.Editada =1, 
			Respuetas.Correcta = Correcta
            where Respuestas.IdRespuestas = IdRespuestas;
	end;
elseif OPC = 'B' then	#baja_logica
	begin
		update Respuestas 
		set Respuestas.Activo = 0
			where Respuestas.IdRespuestas = IdRespuestas;
    end;
end if;

END$$

DELIMITER ;

# ----------------------------------------------------------------------------------

USE `db_ask_n_cook`;
DROP procedure IF EXISTS `sp_PreguntasValoradas`;

DELIMITER $$
USE `db_ask_n_cook`$$
CREATE PROCEDURE `sp_PreguntasValoradas` (
OPC 					char(1),
IdPreguntas_valoradas	INT,
Pregunta 				INT,
Usuario 				INT,
Util 					TINYINT(1),
No_Util 				TINYINT(1)
)
BEGIN
declare IdUSer int;
declare IdQuestion int;

set IdUser = (SELECT Usuarios.IdUsuarios FROM Usuarios WHERE Usuario.Username = Usuario), 
    IdQuestion = (SELECT Preguntas.IdPreguntas FROM Preguntas WHERE Preguntas.Pregunta = Pregunta);

IF OPC = 'I' then	#insert
	begin
		insert into Preguntas_Valoradas (Preguntas_Valoradas.Id_Pregunta, Preguntas_Valoradas.Id_Usuario, 
        Preguntas_Valoradas.Util, Preguntas_Valoradas.No_Util) 
		values (IdQuestion, IdUser, Util, No_Util);    
	end;
elseif OPC = 'D' then	#delete
	begin
		delete
        from Preguntas_Valoradas 
		where Preguntas_Valoradas.IdPreguntas_valoradas = IdPreguntas_valoradas;
    end;
end if;
END$$

DELIMITER ;

# ----------------------------------------------------------------------------------

USE `db_ask_n_cook`;
DROP procedure IF EXISTS `sp_RespuestasValoradas`;

DELIMITER $$
USE `db_ask_n_cook`$$
CREATE PROCEDURE `sp_RespuestasValoradas` (
OPC 					char(1),
IdRespuestas_valoradas	INT,
Respuesta 				INT,
Usuario 				INT,
Util 					TINYINT(1),
No_Util 				TINYINT(1)
)
BEGIN
declare IdUSer int;
declare IdAnswer int;

set IdUser = (SELECT Usuarios.IdUsuarios FROM Usuarios WHERE Usuario.Username = Usuario), 
    IdAnswer = (SELECT Respuestas.IdRespuestas FROM Respuestas WHERE Respuestas.Respuesta = Respuesta);

IF OPC = 'I' then	#insert
	begin
		insert into Respuestas_Valoradas (Respuestas_Valoradas.Id_Respuesta, Respuestas_Valoradas.Id_Usuario, 
        Respuestas_Valoradas.Util, Respuestas_Valoradas.No_Util) 
		values (IdAnswer, IdUser, Util, No_Util);    
	end;
elseif OPC = 'D' then	#delete
	begin
		delete
        from Respuestas_Valoradas 
		where Respuestas_Valoradas.IdRespuestas_valoradas = IdRespuestas_valoradas;
    end;
end if;
END$$

DELIMITER ;

# ----------------------------------------------------------------------------------

USE `db_ask_n_cook`;
DROP procedure IF EXISTS `sp_Favoritas`;

DELIMITER $$
USE `db_ask_n_cook`$$
CREATE PROCEDURE `sp_Favoritas` (
OPC 					char(1),
IdPreguntas_Favoritas 	INT,
Pregunta 				INT,
Usuario 				INT,
Favorito 				TINYINT(1)
)
BEGIN
declare IdUSer int;
declare IdQuestion int;

set IdUser = (SELECT Usuarios.IdUsuarios FROM Usuarios WHERE Usuario.Username = Usuario), 
    IdQuestion = (SELECT Preguntas.IdPreguntas FROM Preguntas WHERE Preguntas.Pregunta = Pregunta);

IF OPC = 'I' then	#insert
	begin
		insert into Preguntas_Favoritas (Preguntas_Favoritas.Id_Pregunta, Preguntas_Favoritas.Id_Usuario, 
        Preguntas_Favoritas.Favorito) 
		values (IdQuestion, IdUser, 1);    
	end;
elseif OPC = 'B' then	#baja_logica
	begin
		update Preguntas_Favoritas 
		set Preguntas_Favoritas.Favorito = 0
			where Preguntas_Favoritas.IdPreguntas_Favoritas = IdPreguntas_Favoritas;
    end;
 end if;   
END$$

DELIMITER ;





